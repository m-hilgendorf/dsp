# README #

### Summary ###

* A small C++ template library of DSP objects. This should primarily serve  
	 for educational purposes.
* version 1.0.0

### Contribution guidelines ###

* follow the JUCE Coding Standard: 
		https://www.juce.com/learn/coding-standards
 
* All pull requests are welcome!

* Any contribution targeting performance optimization
	 must be justified! The bottleneck must be identified
	 and improvements proven with profiling data. 
 
* Avoid linking third-party libraries unless absolutely necessary. 
	 Minimize dependencies. 

* Target platforms are x86-64 and ARM. 
 
* Platform specific optimizations should avoid breaking functionality
	 with other platforms. 
	 
* All container objects need to override std::ostream << () to print their contents. 

### Contact ###

* mike hilgendorf
* mike@hilgendorf.audio